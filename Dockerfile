FROM python:3.7.1-alpine

COPY . /

RUN apk update \
    && apk add --no-cache gcc musl-dev python3-dev libffi-dev openssl-dev \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del gcc musl-dev python3-dev libffi-dev openssl-dev

EXPOSE 443

CMD ["python", "bot.py"]