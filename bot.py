import os
import logging
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

TOKEN = os.environ["TELEGRAM_TOKEN"]
URL = os.environ["URL"]

updater = Updater(token=TOKEN)
dispatcher = updater.dispatcher


def start(bot, update):
    bot.send_message(
        chat_id=update.message.chat_id, text="I'm a bot, please talk to me!"
    )


def help(bot, update, args):
    if len(args) == 0:
        bot.send_message(
            chat_id=update.message.chat_id,
            text=(
                "Use these commands to interact with me:\n"
                "/help Shows this message. Use with a command to show more information\n"
                "/about Display information about my purpose and my creator\n"
                "/startraid Starts the raid planning\n"
                "/raidresults Displays the dates that people have chosen"
            ),
        )
    elif args[0] == "start":
        bot.send_message(
            chat_id=update.message.chat_id,
            text="/start\nThis command starts the bot and allows it to communicate. Only applicable for private chats.",
        )
    elif args[0] == "help":
        bot.send_message(
            chat_id=update.message.chat_id,
            text="/help [command]\nThis command displays all the commands that can be used with the bot.\nWhen used with a command, it displays more detailed information, including correct syntax.",
        )
    elif args[0] == "about":
        bot.send_message(
            chat_id=update.message.chat_id,
            text="/about\nThis command displays information about the bot and its creator.",
        )
    elif args[0] == "startraid":
        bot.send_message(
            chat_id=update.message.chat_id,
            text="/startraid <day1> <day2> ...\nOrders the bot to start raid planning. Days can be in any form of 'DD/MM', or 'Monday'.",
        )
    elif args[0] == "raidresults":
        bot.send_message(
            chat_id=update.message.chat_id,
            text="/raidresults\nShows the results of the last vote.",
        )


def about(bot, update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text="I was designed to help plan house raids by listing the people who are available for a particular day.\nCreator: Wong Yi Xiong\nSource Code: https://gitlab.com/Mintd/house-raiding-bot",
    )


def format_message(chat_data):
    message = ""
    for k, v in chat_data.items():
        if k != "reply_markup":
            message += f"{k}: {' '.join(v)}\n"
    return message


def start_raid(bot, update, args, chat_data):
    if len(args) == 0:
        bot.send_message(
            chat_id=update.message.chat_id,
            text="/startraid requires arguments. Use /help for more info.",
        )
    else:
        inline_keyboard = [[InlineKeyboardButton(s, callback_data=s)] for s in args]
        inline_keyboard.append(
            [InlineKeyboardButton("End Raid", callback_data="endraid")]
        )

        chat_data.clear()
        chat_data.update({day: [] for day in args})
        chat_data["reply_markup"] = InlineKeyboardMarkup(inline_keyboard)

        bot.send_message(
            chat_id=update.message.chat_id,
            text=format_message(chat_data),
            reply_markup=chat_data["reply_markup"],
        )


def button_handler(bot, update, chat_data):
    pressed = update.callback_query.data
    name = update.callback_query.from_user.name

    if pressed == "endraid" and update.callback_query.from_user.id == 99_252_357:
        bot.edit_message_text(
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            text=format_message(chat_data),
        )
        bot.answer_callback_query(update.callback_query.id)

    elif name not in chat_data[pressed]:
        chat_data[pressed].append(name)

        bot.edit_message_text(
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            text=format_message(chat_data),
            reply_markup=chat_data["reply_markup"],
        )
        bot.answer_callback_query(update.callback_query.id)

    elif name in chat_data[pressed]:
        chat_data[pressed].remove(name)

        bot.edit_message_text(
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            text=format_message(chat_data),
            reply_markup=chat_data["reply_markup"],
        )
        bot.answer_callback_query(update.callback_query.id)


def raid_results(bot, update, chat_data):
    if chat_data:
        bot.send_message(chat_id=update.message.chat_id, text=format_message(chat_data))
    else:
        bot.send_message(
            chat_id=update.message.chat_id,
            text="No recent raids were found in my memory",
        )


# fmt: off
dispatcher.add_handler(CommandHandler("start", start))
dispatcher.add_handler(CommandHandler("help", help, pass_args=True))
dispatcher.add_handler(CommandHandler("about", about))
dispatcher.add_handler(CommandHandler("startraid", start_raid, pass_args=True, pass_chat_data=True))
dispatcher.add_handler(CallbackQueryHandler(button_handler, pass_chat_data=True))
# fmt: on

updater.start_webhook(listen="0.0.0.0", port=int(os.environ["PORT"]), url_path=TOKEN)
updater.bot.set_webhook(f"{URL}{TOKEN}")
updater.idle()
